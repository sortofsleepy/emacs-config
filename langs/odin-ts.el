;; Treesitter mode for Odin.
;; Based on mode for C3 by
;; Christian Buttner <https://github.com/cbuttner>
;; https://github.com/c3lang/c3-ts-mode

;; Still working through learning how to do this well; some things may be inefficent or could be done better.
;; this is a good resourc
;; https://archive.casouri.cc/note/2023/tree-sitter-starter-guide/

(require 'treesit)
(require 'c-ts-common)
(add-to-list 'treesit-language-source-alist
	'(odin "https://github.com/tree-sitter-grammars/tree-sitter-odin"))

;;(eval-when-compile (require 'rx))

(declare-function treesit-node-child-by-field-name "treesit.c")
(declare-function treesit-node-type "treesit.c")
(declare-function treesit-node-text "treesit.c")
(declare-function treesit-parser-create "treesit.c")

(defgroup odin-ts nil
  "Major mode for editing Odin files."
  :prefix "odin-ts-"
  :group 'languages)

(defvar odin-ts-mode--syntax-table
  (let ((table (make-syntax-table)))
    ;; Taken from c-ts-mode.el
    (modify-syntax-entry ?_  "_"     table)
    (modify-syntax-entry ?\\ "\\"    table)
    (modify-syntax-entry ?+  "."     table)
    (modify-syntax-entry ?-  "."     table)
    (modify-syntax-entry ?=  "."     table)
    (modify-syntax-entry ?%  "."     table)
    (modify-syntax-entry ?<  "."     table)
    (modify-syntax-entry ?>  "."     table)
    (modify-syntax-entry ?&  "."     table)
    (modify-syntax-entry ?|  "."     table)
    (modify-syntax-entry ?:`  "."    table)
    (modify-syntax-entry ?\' "\""    table)
    (modify-syntax-entry ?`  "\""    table)
    (modify-syntax-entry ?\240 "."   table)
    (modify-syntax-entry ?/ ". 124b" table)
    (modify-syntax-entry ?* ". 23"   table)
    (modify-syntax-entry ?\n "> b"   table)
    (modify-syntax-entry ?\^m "> b"  table)
    table)
  "Syntax table for `odin-ts-mode'.")

(defvar odin-ts-mode--font-lock-settings
  ;; NOTE Earlier rules have precedence over later rules
  (treesit-font-lock-rules

   :language 'odin
   :feature 'comment
   '((comment) @font-lock-comment-face)

   :language 'odin
   :feature 'block-comment
   '((block_comment) @font-lock-comment-face)


   :language 'odin
   :feature 'package
   '(("package") @font-lock-keyword-face)

   ;; for import statements with an alias
   :language 'odin
   :feature 'import-alias-statement
   '((import_declaration
    "import" @font-lock-keyword-face
    alias: (identifier) @font-lock-variable-name-face
    (string
     ["\""] @font-lock-string-face
     (string_content) @font-lock-string-face
     ["\""] @font-lock-string-face)))

   ;; for normal import statements without an alias
   :language 'odin
   :feature 'import-statement
   '((import_declaration
    "import" @font-lock-keyword-face
    (string
     ["\""] @font-lock-string-face
     (string_content) @font-lock-string-face
     ["\""] @font-lock-string-face)))

   :language 'odin
   :feature 'string-statement
   '(
    (string
     ["\""] @font-lock-string-face
     (string_content) @font-lock-string-face
     ["\""] @font-lock-string-face)
    (string
     ["`"] @font-lock-string-face
     (string_content) @font-lock-string-face
     ["`"] @font-lock-string-face)
   )

   :language 'odin
   :feature 'cast-expression
   `((cast_expression "cast" @font-lock-keyword-face
     (type (identifier) @font-lock-variable-name-face)
       ))

   ;; TODO may need to change variable name face though, not sure I like it
   :language 'odin
   :feature 'function-expression
   '((call_expression function: (identifier) @function.call @font-lock-function-call-face))

   :language 'odin
   :feature 'keywords
   `(["defer","dynamic", "::",":=", "return", "&", "struct","=","matrix","true","false","enum"] @font-lock-keyword-face)

   :language 'odin
   :feature 'proc-call
   `(["proc"] @font-lock-function-call-face)

   :language 'odin
   :feature 'operator-call
   `(["for", "if", "else", "or_else", "in"] @font-lock-function-call-face)

   :language 'odin
   :feature 'procedure-declaration
   '((procedure_declaration
     (identifier) @font-lock-keyword-face))

   :language 'odin
   :feature 'overloaded-procedure-declaration
   `((overloaded_procedure_declaration (identifier) @font-lock-keyword-face))


   ;; example
   ;; main :: proc(v:u32)
   :language 'odin
   :feature 'procedure-parameter
   `( (parameter ":" @font-lock-keyword-face
      (type (identifier) @font-lock-constant-face)
       ))

   ;; example
   ;; main :: proc(v:^u32)
   :language 'odin
   :feature 'function-pointer-parameter
   `((pointer_type) @font-lock-variable-name-face)
   ;;`((pointer_type) @font-lock-type-face)

   ;; example
   ;; main :: proc(v:[]u32)
   :language 'odin
   :feature 'function-array-parameter
   `((array_type) @font-lock-variable-name-face)
   ;;`((array_type) @font-lock-type-face)

   ;; example
   ;; main :: proc(v:v.u32)
   :language 'odin
   :feature 'function-field-parameter
   `((field_type) @font-lock-variable-name-face)
   ;;`((field_type) @font-lock-type-face)

    :language 'odin
    :feature 'constant-declaration
    '((const_declaration (identifier) @font-lock-keyword-face))

    :language 'odin
    :feature 'struct-identifier
    '((struct (identifier) @font-lock-keyword-face))

    ;; Covers declarations like
    ;; []u32{} - should highlight the u32 portion
    :language 'odin
    :feature 'struct-identifier-two
    '((type (identifier) @font-lock-variable-name-face))

    ;;;;;; PROPERTIES ;;;;;;;;;;;;
    ;; This is for declarations like
    ;; v.one
    ;; v.one.two
    ;;
    ;; It will be a little weird, but I think it works out well.
    ;; If you have something like v.one.two, two will be in another color.
    ;; May try to clean up later.

    :language 'odin
    :feature 'member-property-one
    '((member_expression
        (member_expression "." (identifier) @font-lock-keyword-face)))

     :language 'odin
     :feature 'member-property-two
     '(("." (identifier) @font-lock-type-face))

    :language 'odin
    :feature 'member-identifier
    '((member_expression (identifier) @font-lock-keyword-face))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    :language 'odin
    :feature 'nil-call
    `(((nil) @font-lock-function-call-face))


   :language 'odin
   :feature 'error
   :override t
   '((ERROR) @font-lock-warning-face))
  "Tree-sitter font-lock settings for `odin-ts-mode'.")

;; Very very basic handling but should be ok enough for now.
;; It's better to use editorconfig or similar, rules are a bit finicky and the documentation isn't
;; great
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Parser_002dbased-Indentation.html
(defvar odin-ts-mode--simple-indent-rules
  `((odin
     ((parent-is "source_file") column-0 0)

     ;;;;; Handle block content, ie things inside a function, etc ;;;;;
     ((parent-is "compound_statement") standalone-parent 4)
     ;; Closing bracket.
     ((node-is "}") standalone-parent 0)
     ;; Opening bracket.
     ((node-is "compound_statement") standalone-parent 4)

     ;; Handle general block definitions - ie within an if statement
     ((parent-is "block") standalone-parent 4)
     ;; Closing bracket.
     ((node-is "}") standalone-parent 0)
     ;; Opening bracket.
     ((node-is "block") standalone-parent 4)

    ;;;;; Similar to above but handles things within {} that's within a block area ;;;;;;;
    ((parent-is "struct") standalone-parent 4)
    ;; Closing bracket.
    ((node-is "}") standalone-parent 0)
    ;; Opening bracket.
    ((node-is "struct") standalone-parent odin-ts-mode-indent-offset)

    ;;;;; Similar to above but handles things within {} that's within a block area ;;;;;;;
    ((parent-is "enum") standalone-parent 4)
    ;; Closing bracket.
    ((node-is "}") standalone-parent 0)
    ;; Opening bracket.
    ((node-is "enum") standalone-parent odin-ts-mode-indent-offset)

     )))

(defun odin-ts-mode--defun-name (node)
  "Return the name of the defun NODE."
  (let ()
    (treesit-node-text
     (treesit-node-child-by-field-name
      (pcase (treesit-node-type node)
        ("func_definition" (treesit-node-child node 1))
        ("macro_declaration" (treesit-node-child node 1))
        (_ node))
      "name")
     t)))


;;;###autoload
(define-derived-mode odin-ts-mode prog-mode "Odin-TS-Mode"
  "Major mode for editing ODIN files, powered by tree-sitter."
  :group 'odin-ts
  :syntax-table odin-ts-mode--syntax-table

  ;; Comment
  (c-ts-common-comment-setup)

  ;; Electric
  (setq-local electric-indent-chars
              (append "{}():;," electric-indent-chars))

  (when (treesit-ready-p 'odin)
    (treesit-parser-create 'odin)

    ;; Font-lock
    (setq-local treesit-font-lock-settings
      odin-ts-mode--font-lock-settings)

    (setq-local treesit-font-lock-feature-list
      '((comment
          package
          block-comment
          import-alias-statement
          import-statement
          string-statement
          cast-expression
          proc-call
          operator-call
          function-expression
          keywords
          keywords-two
          procedure-declaration
          overloaded-procedure-declaration
          procedure-parameter
          function-pointer-parameter
          function-array-parameter
          function-field-parameter
          constant-declaration
          struct-identifier
          struct-identifier-two
          member-property-one
          member-property-two
          member-identifier
          error
          nil-call
      ))
    )

    ;; Indent
    (setq-local treesit-simple-indent-rules odin-ts-mode--simple-indent-rules)

    ;; Navigation
    (setq-local treesit-defun-name-function #'odin-ts-mode--defun-name)

    ;; Imenu - leaving here in case it's useful for someone. Currently it doesn't seem possible to jump to specific
    ;; named instances(everything is "Anonymous"), which as I understand things, is due to the grammar
    ;;(setq-local treesit-simple-imenu-settings
    ;;    `(("Struct" "\\`struct\\'" nil nil)
    ;;        ("Block" "\\`block\\'" nil nil)
    ;;        ("Procedure" "\\`procedure_declaration\\'" nil nil)))

    ;; Which-function
    (setq-local which-func-functions (treesit-defun-at-point))

    (treesit-major-mode-setup)))


(when (treesit-ready-p 'odin)
  (add-to-list 'auto-mode-alist '("\\.odin\\'" . odin-ts-mode)))

(provide 'odin-ts-mode)

(setq odin-ts-mode-indent-offset 4)
(setq treesit-font-lock-level 4)
;;; odin-ts-mode.el ends here

;; Make sure eglot attempts to turns on after we start using odin mode
;; Note that this assumes you have OLS installed
;; https://github.com/DanielGavin/ols
(add-hook 'odin-ts-mode-hook 'eglot-ensure)

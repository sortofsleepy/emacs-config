;; Treesitter mode for GLSL.
;; Based on mode for C3 by
;; Christian Buttner <https://github.com/cbuttner>
;; https://github.com/c3lang/c3-ts-mode

(require 'treesit)
(require 'c-ts-common)
(add-to-list 'treesit-language-source-alist
	'(glsl "https://github.com/tree-sitter-grammars/tree-sitter-glsl"))

;;(eval-when-compile (require 'rx))

(declare-function treesit-node-child-by-field-name "treesit.c")
(declare-function treesit-node-type "treesit.c")
(declare-function treesit-node-text "treesit.c")
(declare-function treesit-parser-create "treesit.c")

(defgroup glsl-ts nil
  "Major mode for editing glsl files."
  :prefix "glsl-ts-"
  :group 'languages)

(defvar glsl-ts-mode--syntax-table
  (let ((table (make-syntax-table)))
    ;; Taken from c-ts-mode.el
    (modify-syntax-entry ?_  "_"     table)
    (modify-syntax-entry ?\\ "\\"    table)
    (modify-syntax-entry ?+  "."     table)
    (modify-syntax-entry ?-  "."     table)
    (modify-syntax-entry ?=  "."     table)
    (modify-syntax-entry ?%  "."     table)
    (modify-syntax-entry ?<  "."     table)
    (modify-syntax-entry ?>  "."     table)
    (modify-syntax-entry ?&  "."     table)
    (modify-syntax-entry ?|  "."     table)
    (modify-syntax-entry ?:`  "."    table)
    (modify-syntax-entry ?\' "\""    table)
    (modify-syntax-entry ?`  "\""    table)
    (modify-syntax-entry ?\240 "."   table)
    (modify-syntax-entry ?/ ". 124b" table)
    (modify-syntax-entry ?* ". 23"   table)
    (modify-syntax-entry ?\n "> b"   table)
    (modify-syntax-entry ?\^m "> b"  table)
    table)
  "Syntax table for `glsl-ts-mode'.")

(defvar glsl-ts-mode--font-lock-settings
  ;; NOTE Earlier rules have precedence over later rules
  (treesit-font-lock-rules

   :language 'glsl
   :feature 'preprocessor
   '((preproc_call) @font-lock-string-face)

   :language 'glsl
   :feature 'preprocessor-extension
   '((preproc_extension) @font-lock-string-face)

   :language 'glsl
   :feature 'preprocessor-directive
   '((preproc_directive) @font-lock-string-face)

   :language 'glsl
   :feature 'primitives
   `((primitive_type) @font-lock-variable-name-face)

   :language 'glsl
   :feature 'type-identifier
   `((type_identifier) @font-lock-variable-name-face)

   :language 'glsl
   :feature 'argument-list
   `(("," (argument_list)  @font-lock-variable-name-face))

   :language 'glsl
   :feature 'function-call
   `((call_expression) function: (identifier) @font-lock-function-call-face)

   :language 'glsl
   :feature 'boolean
   '((true) @font-lock-keyword-face
    (false) @font-lock-keyword-face)


   :language 'glsl
   :feature 'fields
   '((field_identifier) @font-lock-keyword-face)

   ;;;;;;;; KEYWORDS ;;;;;;;;;;

   ;; for some reason we can't use the list syntax but have to specify things individually :(
   ;; Will change if I ever get the will to figure it out, this is fine for now since there aren't a bunch
   ;; and some are covered by other rules.

   ;; Covers keywords that are assigned something, ie gl_Position
   :language 'glsl
   :feature 'keyword-position
   `((assignment_expression
      left: (identifier) @font-lock-keyword-face
      (:equal @font-lock-keyword-face "gl_Position")
   ))

   ;; Covers gl_PointSize
   :language 'glsl
   :feature 'keyword-point-size
   `((assignment_expression
      left: (identifier) @font-lock-keyword-face
      (:equal @font-lock-keyword-face "gl_PointSize")
   ))

   ;; Make sure "uniform" is highlighted
   :language 'glsl
   :feature 'keyword-uniform
   `((("uniform") @font-lock-keyword-face))

   :language 'glsl
   :feature 'keyword-out
   `(("out") @font-lock-keyword-face)

   :language 'glsl
   :feature 'layout-spec
   `((("layout")  @font-lock-keyword-face))

   :language 'glsl
   :feature 'layout-spec-value
   `((qualifier) (identifier) (number_literal) @font-lock-string-face)

  )

  "Tree-sitter font-lock settings for `glsl-ts-mode'.")

;; Very very basic handling but should be ok enough for now.
;; It's better to use editorconfig or similar, rules are a bit finicky and the documentation isn't
;; great
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Parser_002dbased-Indentation.html
(defvar glsl-ts-mode--simple-indent-rules
  `((glsl
     ((parent-is "source_file") column-0 0)

     ;; Closing bracket.
     ((node-is "}") standalone-parent 0)

     ;; Handle general block definitions - ie within an if statement
     ((parent-is "compound_statement") standalone-parent 4)

     ;; Opening bracket.
     ((node-is "compound_statement") standalone-parent 4)


     )))

(defun glsl-ts-mode--defun-name (node)
  "Return the name of the defun NODE."
  (let ()
    (treesit-node-text
     (treesit-node-child-by-field-name
      (pcase (treesit-node-type node)
        ("func_definition" (treesit-node-child node 1))
        ("macro_declaration" (treesit-node-child node 1))
        (_ node))
      "name")
     t)))


;;;###autoload
(define-derived-mode glsl-ts-mode prog-mode "glsl-TS-Mode"
  "Major mode for editing glsl files, powered by tree-sitter."
  :group 'glsl-ts
  :syntax-table glsl-ts-mode--syntax-table

  ;; Comment
  (c-ts-common-comment-setup)

  ;; Electric
  (setq-local electric-indent-chars
              (append "{}():;," electric-indent-chars))

  (when (treesit-ready-p 'glsl)
    (treesit-parser-create 'glsl)

    ;; Font-lock
    (setq-local treesit-font-lock-settings
      glsl-ts-mode--font-lock-settings)

    (setq-local treesit-font-lock-feature-list
      '((
          boolean
          preprocessor
          preprocessor-extension
          preprocessor-directive
          primitives
          fields
          function-call
          argument-list
          type-identifier
          keyword-out
          keyword-position
          keyword-point-size
          keyword-uniform
          layout-spec
          layout-spec-value
      ))
    )

    ;; Indent
    (setq-local treesit-simple-indent-rules glsl-ts-mode--simple-indent-rules)

    ;; Navigation
    (setq-local treesit-defun-name-function #'glsl-ts-mode--defun-name)

    ;; Imenu - leaving here in case it's useful for someone. Currently it doesn't seem possible to jump to specific
    ;; named instances(everything is "Anonymous"), which as I understand things, is due to the grammar
    ;;(setq-local treesit-simple-imenu-settings
    ;;    `(("Struct" "\\`struct\\'" nil nil)
    ;;        ("Block" "\\`block\\'" nil nil)
    ;;        ("Procedure" "\\`procedure_declaration\\'" nil nil)))

    ;; Which-function
    (setq-local which-func-functions (treesit-defun-at-point))

    (treesit-major-mode-setup)))


(when (treesit-ready-p 'glsl)
  (add-to-list 'auto-mode-alist '("\\.vert\\'" . glsl-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.glsl\\'" . glsl-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.comp\\'" . glsl-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.frag\\'" . glsl-ts-mode)))

(provide 'glsl-ts-mode)

;; fro debugging
;;(setq treesit--indent-verbose t)
;;(setq treesit--font-lock-verbose t)

(setq glsl-ts-mode-indent-offset 4)
(setq treesit-font-lock-level 4)
;;; glsl-ts-mode.el ends here

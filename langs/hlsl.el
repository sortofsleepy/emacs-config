;; Treesitter mode for hlsl.
;; Based on mode for C3 by
;; Christian Buttner <https://github.com/cbuttner>
;; https://github.com/c3lang/c3-ts-mode

;; Still working through learning how to do this well; some things may be inefficent or could be done better.
;; this is a good resourc
;; https://archive.casouri.cc/note/2023/tree-sitter-starter-guide/

(require 'treesit)
(require 'c-ts-common)


;;(eval-when-compile (require 'rx))

(declare-function treesit-node-child-by-field-name "treesit.c")
(declare-function treesit-node-type "treesit.c")
(declare-function treesit-node-text "treesit.c")
(declare-function treesit-parser-create "treesit.c")

(defgroup hlsl-ts nil
  "Major mode for editing hlsl files."
  :prefix "hlsl-ts-"
  :group 'languages)

(defvar hlsl-ts-mode--syntax-table
  (let ((table (make-syntax-table)))
    ;; Taken from c-ts-mode.el
    (modify-syntax-entry ?_  "_"     table)
    (modify-syntax-entry ?\\ "\\"    table)
    (modify-syntax-entry ?+  "."     table)
    (modify-syntax-entry ?-  "."     table)
    (modify-syntax-entry ?=  "."     table)
    (modify-syntax-entry ?%  "."     table)
    (modify-syntax-entry ?<  "."     table)
    (modify-syntax-entry ?>  "."     table)
    (modify-syntax-entry ?&  "."     table)
    (modify-syntax-entry ?|  "."     table)
    (modify-syntax-entry ?:`  "."    table)
    (modify-syntax-entry ?\' "\""    table)
    (modify-syntax-entry ?`  "\""    table)
    (modify-syntax-entry ?\240 "."   table)
    (modify-syntax-entry ?/ ". 124b" table)
    (modify-syntax-entry ?* ". 23"   table)
    (modify-syntax-entry ?\n "> b"   table)
    (modify-syntax-entry ?\^m "> b"  table)
    table)
  "Syntax table for `hlsl-ts-mode'.")

(defvar hlsl-ts-mode--font-lock-settings
  ;; NOTE Earlier rules have precedence over later rules
  (treesit-font-lock-rules

   :language 'hlsl
   :feature 'comment
   '((comment) @font-lock-comment-face)


   ;; handle variables
   :language 'hlsl
   :feature 'vars
   '((declaration type: (type_identifier) @font-lock-variable-name-face))

   ;; function calling
   :language 'hlsl
   :feature 'func-call
   '((expression_statement
       (call_expression function: (identifier) @font-lock-function-call-face)))

   :language 'hlsl
   :feature 'function-declaration
   '((function_declarator
       declarator: (identifier) @font-lock-function-call-face))

   :language 'hlsl
   :feature 'parameter-declaration
   '((function_declarator
       declarator: (identifier) @font-lock-function-call-face
       parameters:
        (parameter_list (
            (parameter_declaration type: (type_identifier)  @font-lock-function-call-face)))))

   ;; NOT WORKING YET
   :language 'hlsl
   :feature 'semantics
   '(sematics: (identifier) @font-lock-function-call-face)

   :language 'hlsl
   :feature 'lambda
   '(declarator:
     (template_function name: (identifier) @font-lock-function-call-face))

   :language 'hlsl
   :feature 'error
   :override t
   '((ERROR) @font-lock-warning-face))
  "Tree-sitter font-lock settings for `hlsl-ts-mode'.")

;; Very very basic handling but should be ok enough for now.
;; It's better to use editorconfig or similar, rules are a bit finicky and the documentation isn't
;; great
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Parser_002dbased-Indentation.html
(defvar hlsl-ts-mode--simple-indent-rules
  `((hlsl
     ((parent-is "source_file") column-0 0)

     ;;;;; Handle block content, ie things inside a function, etc ;;;;;
     ((parent-is "compound_statement") standalone-parent 4)
     ;; Closing bracket.
     ((node-is "}") standalone-parent 0)
     ;; Opening bracket.
     ((node-is "compound_statement") standalone-parent 4)

     ;; Handle general block definitions - ie within an if statement
     ((parent-is "block") standalone-parent 4)
     ;; Closing bracket.
     ((node-is "}") standalone-parent 0)
     ;; Opening bracket.
     ((node-is "block") standalone-parent 4)

    ;;;;; Similar to above but handles things within {} that's within a block area ;;;;;;;
    ((parent-is "struct") standalone-parent 4)
    ;; Closing bracket.
    ((node-is "}") standalone-parent 0)
    ;; Opening bracket.
    ((node-is "struct") standalone-parent hlsl-ts-mode-indent-offset)

    ;;;;; Similar to above but handles things within {} that's within a block area ;;;;;;;
    ((parent-is "enum") standalone-parent 4)
    ;; Closing bracket.
    ((node-is "}") standalone-parent 0)
    ;; Opening bracket.
    ((node-is "enum") standalone-parent hlsl-ts-mode-indent-offset)

     )))

(defun hlsl-ts-mode--defun-name (node)
  "Return the name of the defun NODE."
  (let ()
    (treesit-node-text
     (treesit-node-child-by-field-name
      (pcase (treesit-node-type node)
        ("func_definition" (treesit-node-child node 1))
        ("macro_declaration" (treesit-node-child node 1))
        (_ node))
      "name")
     t)))


;;;###autoload
(define-derived-mode hlsl-ts-mode prog-mode "hlsl-TS-Mode"
  "Major mode for editing hlsl files, powered by tree-sitter."
  :group 'hlsl-ts
  :syntax-table hlsl-ts-mode--syntax-table

  ;; Comment
  (c-ts-common-comment-setup)

  ;; Electric
  (setq-local electric-indent-chars
              (append "{}():;," electric-indent-chars))

  (when (treesit-ready-p 'hlsl)
    (treesit-parser-create 'hlsl)

    ;; Font-lock
    (setq-local treesit-font-lock-settings
      hlsl-ts-mode--font-lock-settings)

    (setq-local treesit-font-lock-feature-list
      '((

          comment
          vars
          func-call
          function-declaration
          parameter-declaration
          semantics
          lambda
      ))
    )

    ;; Indent
    (setq-local treesit-simple-indent-rules hlsl-ts-mode--simple-indent-rules)

    ;; Navigation
    (setq-local treesit-defun-name-function #'hlsl-ts-mode--defun-name)

    ;; Imenu - leaving here in case it's useful for someone. Currently it doesn't seem possible to jump to specific
    ;; named instances(everything is "Anonymous"), which as I understand things, is due to the grammar
    ;;(setq-local treesit-simple-imenu-settings
    ;;    `(("Struct" "\\`struct\\'" nil nil)
    ;;        ("Block" "\\`block\\'" nil nil)
    ;;        ("Procedure" "\\`procedure_declaration\\'" nil nil)))

    ;; Which-function
    (setq-local which-func-functions (treesit-defun-at-point))

    (treesit-major-mode-setup)))


(when (treesit-ready-p 'hlsl)
  (add-to-list 'auto-mode-alist '("\\.hlsl\\'" . hlsl-ts-mode))
  ;;(add-to-list 'auto-mode-alist '("\\.slang\\'" . hlsl-ts-mode))
)

;; setup file associations
(add-to-list 'auto-mode-alist '("\\.slang\\'" . c++-mode))

(provide 'hlsl-ts-mode)

;; fro debugging
;;(setq treesit--indent-verbose t)
;;(setq treesit--font-lock-verbose t)

(setq hlsl-ts-mode-indent-offset 4)
(setq treesit-font-lock-level 4)
;;; hlsl-ts-mode.el ends here

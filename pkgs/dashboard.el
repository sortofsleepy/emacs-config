;;; This file is responsile for the dashboard that shows up when emacs is first loaded.
;;;
(use-package dashboard)
(require 'dashboard)
(dashboard-setup-startup-hook)
;; set startup banner.
;; Possible values
;; 1. nil - no banner
;; 2. official - displays emacs logo
;; 3. logo - displays alt emacs logo
;; 4. - 1, 2 or 3 which displays one of the text banners
;; 5. "path/to/your/image.gif", "path/to/your/image.png", "path/to/your/text.txt" or "path/to/your/image.xbm" which displays whatever gif/image/text/xbm you would prefer
;; 6. a cons of '("path/to/your/image.png" . "path/to/your/text.txt")
(setq dashboard-startup-banner 3)
;; set title
(setq dashboard-banner-logo-title "Welcome!")
;; center
(setq dashboard-center-content t)

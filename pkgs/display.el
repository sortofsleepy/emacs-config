;; This file manages the general display settings including window size and font.

;; Explicitly define a width to reduce the cost of on-the-fly computation
(setq-default display-line-numbers-width 3)

;; Show absolute line numbers for narrowed regions to make it easier to tell the
;; buffer is narrowed, and where you are, exactly.
(setq-default display-line-numbers-widen t)

(setq display-line-numbers-mode "realtive")


(setq custom--inhibit-theme-enable nil)

;; Add startup hook to try and make sure we don't ingest default Emacs styles
(add-hook 'emacs-startup-hook
  (lambda ()
    (set-face-attribute 'line-number nil
                        :background nil
                        :foreground "#585858"))

)

;; TODO it'd be nice to adjust based on window size but for now this is fine. Should be roughly a little over/under 1280x920(emacs measures in
;; terms of columns/rows vs pixels)
;;(add-to-list 'initial-frame-alist '(width . 160))
;;(add-to-list 'initial-frame-alist '(height . 60))

;; doom themes
;; https://github.com/doomemacs/themes
;; TODO maybe just have a themes folder. But for now this provides a good variety.
(use-package doom-themes
             :config
             ;; Global settings (defaults)
             (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
                   doom-themes-enable-italic t) ; if nil, italics is universally disabled

             (load-theme 'doom-badger t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

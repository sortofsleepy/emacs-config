;; rewrite some keybinds to be a little simpler
;; isearch (originally C-s when in "insert" mode)
(global-set-key (kbd "C-q") 'isearch-forward)
(define-key isearch-mode-map "\C-q" 'isearch-repeat-forward)

;; rebind write-file (originally ESC-w)
;;(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-s")
(lambda ()
(interactive)
    (save-buffer)
    (editorconfig-apply)
))

;; rebind select all (originally C-x h)
(global-set-key (kbd "C-a") 'mark-whole-buffer)

;; rebind find-replace (originally M-%)
;;(global-set-key (kbd "C-f") 'query-replace)

;; Make it simpler to move between splits
(global-set-key (kbd "C-h") 'windmove-left)
(global-set-key (kbd "C-l") 'windmove-right)
(global-set-key (kbd "C-k") 'windmove-up)
(global-set-key (kbd "C-j") 'windmove-down)

;; file fuzzy findingr
(global-set-key (kbd "C-c f") 'project-find-file)

;; ripgrep for project search (so we don't have to specify the path every time)
(global-set-key (kbd "C-c r") 'rg-project)



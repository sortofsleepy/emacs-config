emacs --batch -l org "modules/early-init.org" -f org-babel-tangle
emacs --batch -l org "modules/init.org" -f org-babel-tangle

mv modules/early-init.el ~/.emacs.d/early-init.el
mv modules/init.el ~/.emacs.d/init.el

